import socket
import select
import signal
import comm
import sys
import json
import time
import math
import collections
import netifaces as ni
import os
import shutil
import hashlib
from threading import Thread
from time import sleep
from threading import Lock, Thread

class node(object):

	def __init__(self, host_ip, n_num, port=1234, backlog=16):
		# create node socket
		self.nodeFd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.nodeFd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.nodeFd.bind(("", port))
		self.nodeFd.listen(backlog)

		self.lock = Lock()
		self.fingerTable = {}	

		# key interrupt 
		signal.signal(signal.SIGINT, self.sigHandler)

		# connect to the registration server
		self.regFd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.regFd.connect((host_ip, port))
		self.regFd.settimeout(400)

		self.nodeNum = int(n_num)
		self.writeFileNum = 0
		self.localIP = ""		
		self.storedFile = {}
		self.tmpFileFd = None

		try:
			self.localIP = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
			msg = {"cmd":"connect", "ip":self.localIP, "node":self.nodeNum}
			json_msg = json.dumps(msg)
			comm.send(self.regFd, json_msg)
		except socket.error, errMsg:
			print "Couldnt connect with the socket-server\n"	

	def sigHandler(self, signum, frame):
		print "program closed!"
		self.nodeFd.close()
		self.regFd.close()

		# remove node from the chord network
		if self.fingerTable[0]["successor"] != self.nodeNum:
			storeNodeIP = self.fingerTable[0]["ip"]		
			for nodeNum in self.storedFile:
				for fileName in self.storedFile[nodeNum]:
					msg = {
					"cmd":"init_file",
					"targetNode":int(nodeNum),
					"fileName":fileName
					}

					self.sendMessageToNode(storeNodeIP, msg)
					self.sendFile(nodeNum, fileName, storeNodeIP, "yes", True)
					
				os.system("rm -rf ./database/"+str(nodeNum))
		
		sleep(0.2)
		sys.exit()

	def sendMessageToNode(self, addr, msg, port=1234):
		try:
			clntFd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			clntFd.connect((addr, port))
			m = json.dumps(msg)
			clntFd.settimeout(400)
			comm.send(clntFd, m)
			sleep(0.2)
			clntFd.close()
		except socket.error, errMsg:
			print "Couldnt connect with the socket-server\n"
	
	def getSuccessor(self, numOfNode, nodeK, ipTable):
		retNodeNum = -1
		for i in range(numOfNode):
			if ((nodeK + i) % numOfNode) in ipTable:
				retNodeNum = (nodeK + i) % numOfNode
				break

		return retNodeNum
		 
	def getFingerTable(self, numOfNode, ipTable):
		ipTable = {int(k):v for k,v in ipTable.items()}
		numOfEntry = int(math.log(numOfNode, 2))
		self.fingerTable = {}

		for i in range(numOfEntry):
			nodeK = int(self.nodeNum + math.pow(2, i)) % numOfNode
			successor = self.getSuccessor(numOfNode, nodeK, ipTable)
			self.fingerTable[i] = {
			"k+2^i":nodeK, 
			"successor":successor,
			"ip":ipTable[successor]["ip"],
			"hash":ipTable[successor]["hash"]		
			}
	
	def nodeInRange(self, startNode, endNode, targetNode):
		if ( endNode < startNode ):
			endNode = endNode + 16
		
		if ( targetNode < startNode):
			targetNode = targetNode + 16

		return (targetNode > startNode and targetNode <= endNode)

	def handleFileRequest(self, insertNodeIP, targetNode, subCommand, isLocal):
		respCommand = subCommand.split(" ")

		if respCommand[0] == "add_file":
			if int(targetNode) in self.storedFile:
				if respCommand[1] in self.storedFile[int(targetNode)]:
					self.storedFile[int(targetNode)][respCommand[1]]["fileFd"] = None
				else:
					self.storedFile[int(targetNode)].update({respCommand[1]:{"fileFd":None}})
			else:
				self.storedFile[int(targetNode)] = { respCommand[1]:{"fileFd":None} }
			
			if isLocal == True:
				if not os.path.exists("./database/"+str(targetNode)):
					os.makedirs("./database/"+str(targetNode))
				shutil.copy2(respCommand[1], "./database/"+str(targetNode)+"/"+respCommand[1])
				print "file is added to the database"
			else:
				msg = {
				"cmd":"query_file",
				"storeNode":self.nodeNum,
				"storeNodeIP":self.localIP,
				"targetNode":int(targetNode),
				"fileName":respCommand[1]
				}

				self.sendMessageToNode(insertNodeIP, msg)

		elif respCommand[0] == "get_file":
			if int(targetNode) in self.storedFile:
				if respCommand[1] in self.storedFile[int(targetNode)]:
					if isLocal == True:
						self.displayFileContent("./database/"+str(targetNode)+"/"+respCommand[1])
					else:
						self.sendFile(targetNode, respCommand[1], insertNodeIP, "no")
				else:
					if isLocal == True:
						print "file not available"
					else:
						msg = {"cmd":"error", "errorMsg":"file not available"}
						self.sendMessageToNode(insertNodeIP, msg)	
			else:
				if isLocal == True:
					print "file not available"
				else:
					msg = {"cmd":"error", "errorMsg":"file not available"}
					self.sendMessageToNode(insertNodeIP, msg)
		

	def queryNodeFilesList(self, insertNode, insertNodeIP, sendNode, targetNode, subCommand):
		print "query node#%d" % (targetNode)
		startNode = self.nodeNum
		queryNodeEntry = 0

		if sendNode != self.nodeNum:
			if self.nodeInRange(sendNode, self.nodeNum, targetNode): #targetNode == startNode
				if startNode == insertNode:
					print "Target node is itself. Make no connection 1"
					self.handleFileRequest(insertNodeIP, targetNode, subCommand, True)
				else:
					print "Send information from node#%d to node#%d" % (startNode, insertNode)
					self.handleFileRequest(insertNodeIP, targetNode, subCommand, False)
				return

		elif sendNode == insertNode:
			if targetNode == self.nodeNum:
				print "Target node is itself. Make no connection 2"
				self.handleFileRequest(insertNodeIP, targetNode, subCommand, True)
				return

		for i in self.fingerTable:
			if ( self.nodeInRange(self.nodeNum, self.fingerTable[i]["successor"], targetNode) ):
				break
			queryNodeEntry = i
		print "Next search query entry[%d] => Form connection with Node#%d" % (queryNodeEntry, self.fingerTable[queryNodeEntry]["successor"])
		
		if self.fingerTable[queryNodeEntry]["successor"] == self.nodeNum :
			print "Target node is itself. Make no connection 3"
			self.handleFileRequest(insertNodeIP, targetNode, subCommand, True)
		else:
			msg = {
			"cmd":"query", 
			"insertNode":insertNode,
			"insertNodeIP":insertNodeIP,
			"sendNode":self.nodeNum,
			"targetNode":targetNode,
			"subCommand":subCommand }
			
			self.sendMessageToNode(self.fingerTable[queryNodeEntry]["ip"], msg)

	def sendFile(self, targetNode, fileName, ip, isInsert, allocateFile=False, port=1234):
		try:
			clntFd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			clntFd.connect((ip, port))
			clntFd.settimeout(400)
			filePath = ""			

			if isInsert == "yes":
				if allocateFile:
					filePath = "./database/" + str(targetNode) + "/" + fileName
				else:
					filePath = "./" + fileName
			else:
				filePath = "./database/" + str(targetNode) + "/" + fileName
			
			f = open(filePath, 'rb')

			msg = {
			"cmd":"query_file_resp", 
			"fileName":fileName, 
			"targetNode":targetNode, 
			"action":"start", 
			"isInsert":isInsert }
			json_msg = json.dumps(msg)
			comm.send(clntFd, json_msg)

			while True:
				fdata = f.read(1024)
				if fdata:
					msg = {
					"cmd":"query_file_resp", 
					"fileName":fileName, 
					"targetNode":targetNode, 
					"action":"write", 
					"fdata":fdata,
					"isInsert":isInsert }
					json_msg = json.dumps(msg)
					comm.send(clntFd, json_msg)
				else:
					break

			msg = {
			"cmd":"query_file_resp", 
			"fileName":fileName, 
			"targetNode":targetNode, 
			"action":"end",
			"isInsert":isInsert }
			json_msg = json.dumps(msg)
			comm.send(clntFd, json_msg)

			f.close()
		except socket.error, errMsg:
			print "Couldnt connect with the socket-node\n"

	def displayFileContent(self, filePath):
		f = open(filePath, 'rb')
		while True:
			fdata = f.read(1024)
			if fdata:
				print fdata
			else:
				break
		f.close()

	def receiveFile(self, targetNode, action, fileName, isInsert, fData=None):
		if not os.path.exists("./database/"+str(targetNode)):
			os.makedirs("./database/"+str(targetNode))
		
		if action == "start":
			print "File Open -> start downloading"
			if isInsert == "yes":
				self.storedFile[targetNode][fileName]["fileFd"] = open(("./database/"+str(targetNode)+"/"+fileName), 'wb')
			else:
				self.tempFileFd = open(("./tmp/"+fileName), 'wb')
	
		elif action == "write":
			print "File Writing -> file transferring"
			if isInsert == "yes":	
				self.storedFile[targetNode][fileName]["fileFd"].write(fData)
			else:
				self.tempFileFd.write(fData)
		
		elif action == "end":
			print "File Close -> finished downloading"
			if isInsert == "yes":
				self.storedFile[targetNode][fileName]["fileFd"].close()
			else:
				self.tempFileFd.close()
				self.displayFileContent("./tmp/"+fileName)
	
	def allocateFile(self, sendNode, storeNode, storeNodeIP):
		if storeNode < sendNode:
			storeNode = storeNode + 16

		while True:
			sendNode = sendNode + 1
			if sendNode <= storeNode:
				tempNodeNum = sendNode%16
				if tempNodeNum in self.storedFile:
					for fileName in self.storedFile[tempNodeNum]:
						msg = {
						"cmd":"init_file",
						"targetNode":int(tempNodeNum),
						"fileName":fileName
						}

						self.sendMessageToNode(storeNodeIP, msg)
						self.sendFile(tempNodeNum, fileName, storeNodeIP, "yes", True)
					
				if tempNodeNum in self.storedFile:
					for fileName in self.storedFile[tempNodeNum]:
						os.system("rm -rf ./database/"+str(tempNodeNum))
						self.storedFile.pop(tempNodeNum, None)
					
			else:
				break

	def getHash(self, msg):
		return hashlib.sha224(msg).hexdigest()

	def createFile(self, fileName, fileContent):
		f = open(fileName, 'wb')
		f.write(fileContent)
		f.close()
		
	def processTcpMessage(self, message):
		cmd = message.get("cmd")

		if cmd == "update":
			ipTable = message.get("table")
			action = message.get("action")
			nodeNum = message.get("nodeNum")
			
			oldNextNodeSuccessor = -1

			if action == "add":
				if bool(self.fingerTable):
					oldNextNodeSuccessor = self.fingerTable[0]["successor"]
					oldNextNodeSuccessorIP = self.fingerTable[0]["ip"]

			self.getFingerTable(16, ipTable)

			if oldNextNodeSuccessor != -1:
				if self.fingerTable[0]["successor"] == nodeNum and oldNextNodeSuccessor != nodeNum:
					msg = {
					"cmd":"allocate_file",
					"sendNode":self.nodeNum,
					"storeNode":self.fingerTable[0]["successor"],
					"storeNodeIP":self.fingerTable[0]["ip"]
					}
					
					sleep(0.2)
					self.sendMessageToNode(oldNextNodeSuccessorIP, msg)
		
		elif cmd == "init_file":
			targetNode = message.get("targetNode")
			fileName = message.get("fileName")

			if int(targetNode) in self.storedFile:
				if fileName in self.storedFile[int(targetNode)]:
					self.storedFile[int(targetNode)][fileName]["fileFd"] = None
				else:
					self.storedFile[int(targetNode)].append({fileName:{"fileFd":None}})
			else:
				self.storedFile[int(targetNode)] = { fileName:{"fileFd":None} }

		elif cmd == "allocate_file":
			print "re-allocate files to new node"
			sendNode = message.get("sendNode")
			storeNode = message.get("storeNode")
			storeNodeIP = message.get("storeNodeIP")

			self.allocateFile(sendNode, storeNode, storeNodeIP)

		elif cmd == "query":
			insertNode = message.get("insertNode")
			insertNodeIP = message.get("insertNodeIP")
			sendNode = message.get("sendNode")
			targetNode = message.get("targetNode")
			subCommand = message.get("subCommand")
			self.queryNodeFilesList(insertNode, insertNodeIP, sendNode, targetNode, subCommand)

		elif cmd == "query_file_resp":
			fileName = message.get("fileName")
			action = message.get("action")
			isInsert = message.get("isInsert")
			targetNode = message.get("targetNode")
			if action == "write":
				self.receiveFile(targetNode, action, fileName, isInsert, message.get("fdata"))
			else:
				self.receiveFile(targetNode, action, fileName, isInsert)

		elif cmd == "query_file":
			targetNode = int(message.get("targetNode"))
			storeNodeIP = message.get("storeNodeIP")
			fileName = message.get("fileName")
			self.sendFile(targetNode, fileName, storeNodeIP, "yes")

		elif cmd == "error":
			errorMsg = message.get("errorMsg")
			print errorMsg
			
	def processStdinMessage(self, message):
		cmd = message[0]
		if (cmd == "table"):
			for i in self.fingerTable:
				print "Entry[%d] K+2^i:%d Successor:%d Hash:%s IP:%s" % (i, self.fingerTable[i]["k+2^i"], self.fingerTable[i]["successor"], self.fingerTable[i]["hash"], self.fingerTable[i]["ip"])

		elif (cmd == "get_file"):
			queryNode = int(message[1])
			fileName = self.getHash(message[2])+".txt"
			self.queryNodeFilesList(self.nodeNum, self.localIP, self.nodeNum, queryNode, (cmd+" "+ fileName))

		elif (cmd == "add_file" ):
			queryNode = int(message[1])
			fileName = message[2]
			fileContent = message[3]
			fileName = self.getHash(fileName)+".txt"
			self.createFile(fileName, fileContent)
			print "The File is generated:" + str(fileName)
			self.queryNodeFilesList(self.nodeNum, self.localIP, self.nodeNum, queryNode, (cmd+" "+ fileName))

		elif (cmd == "show_file"):
			for nodeNum in self.storedFile:
				for fileName in self.storedFile[nodeNum]:
					print "Node:%d, File:%s" % (nodeNum, fileName)
		
	def nodeServer(self):
		inputs = [sys.stdin, self.regFd, self.nodeFd]		
		stdoutFlag = True		

		while True:
			if stdoutFlag == True:
				sys.stdout.write("@node#"+ str(self.nodeNum) +":")
				sys.stdout.flush()
			
			inFds, outFds, errFds = select.select(inputs, [], [])
			for fd in inFds:
				if fd == sys.stdin:
					cmd = sys.stdin.readline().strip()
					sys.stdout.flush()
					sendData=cmd.split(" ")
					self.processStdinMessage(sendData)

				elif fd == self.nodeFd:
					clntFd, clntAddr = self.nodeFd.accept()
					inputs.append(clntFd)
					stdoutFlag = False

				else:
					self.lock.acquire()
					try:
						recData = comm.receive(fd)
						if recData:
							data = json.loads(recData)
							self.processTcpMessage(data)
							stdoutFlag = True
						else:
							inputs.remove(fd)
					except socket.error:
						inputs.remove(fd)
						continue
					self.lock.release()

if __name__ == "__main__":
	if os.path.exists("./database"):
		os.system("rm -rf ./database")
	os.makedirs("./database")

	if os.path.exists("./tmp"):
		os.system("rm -rf ./tmp")
	os.makedirs("./tmp")

	n = node(sys.argv[1], sys.argv[2])
	n.nodeServer()

		










