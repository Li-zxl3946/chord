import socket
import select
import signal
import comm
import sys
import json
import time
import hashlib
from threading import Thread
from time import sleep
from threading import Lock, Thread
from thPool import ThreadPool

class server(object):
	def __init__(self, port=1234, backlog=16):
		# create socket
		self.serverFd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# set socket option
		self.serverFd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.serverFd.bind(("", port))
		self.serverFd.listen(backlog)

		self.OnlineNodeMap = {}
		self.clientIpTable = {}

		# Construct worker pool 
		self.workerPool = ThreadPool(backlog)

		self.lock = Lock()	
	
		# key interrupt 
		signal.signal(signal.SIGINT, self.sigHandler)

	def sigHandler(self, signum, frame):
		print "program closed!"
		self.serverFd.close()
		sys.exit()

	def getHash(self, msg):
		return hashlib.sha224(msg).hexdigest()

	def processTcpMessage(self, fd, message):
		cmd = message.get('cmd')

		if cmd == "connect":
			nodeNum = message.get('node')
			nodeIP = message.get('ip')
			if nodeNum in self.clientIpTable:
				self.clientIpTable[nodeNum]["fd"] = fd
				self.clientIpTable[nodeNum]["status"] = "ON"
			else:
				self.clientIpTable[nodeNum]={"ip":nodeIP, "fd":fd ,"status":"ON"}

			self.OnlineNodeMap[nodeNum]	= {"ip":nodeIP, "hash":self.getHash(str(nodeNum))}		
			print "Node#%d is connected to the server" % (nodeNum)
			self.sendIpTable("add", nodeNum)

	def sendIpTable(self, action, nodeNum):
		msg = {"cmd":"update", "table":self.OnlineNodeMap, "action":action, "nodeNum":nodeNum}
		json_msg = json.dumps(msg)

		for nodeNum in self.OnlineNodeMap:
			self.workerPool.add_task(comm.send, self.clientIpTable[nodeNum]["fd"], json_msg)

	def processStdinMessage(self, message):
		cmd = message[0]
		if (cmd == "table"):
			for nodeNum in self.clientIpTable:
				print ( "Node#%d IP:%s STATUS:%s Hash:%s"%(nodeNum, 
				self.clientIpTable[nodeNum]["ip"], 
				self.clientIpTable[nodeNum]["status"]),
				self.OnlineNodeMap[nodeNum]["hash"] )

	def serverLoop(self):
		time.sleep(0.01)
		inputs = [sys.stdin, self.serverFd]		
		stdoutFlag = True

		while True:
			if stdoutFlag == True:
				sys.stdout.write("@server:")
				sys.stdout.flush()				

			inFds, outFds, errFds = select.select(inputs, [], [])
			for fd in inFds:
				if fd == sys.stdin:
					cmd = sys.stdin.readline().strip()
					sys.stdout.flush()
					self.lock.acquire()
					self.processStdinMessage(cmd.split(" "))
					self.lock.release()					

				elif fd == self.serverFd:
					clntFd, clntAddr = self.serverFd.accept()
					inputs.append(clntFd)
					stdoutFlag = False

				else:
					self.lock.acquire()
					try:
						recData = comm.receive(fd)
						if recData:
							data = json.loads(recData)
							self.processTcpMessage(fd, data)
							stdoutFlag = True
						else:
							inputs.remove(fd)
							for nodeNum in self.clientIpTable:
								if self.clientIpTable[nodeNum]["fd"] == fd and self.clientIpTable[nodeNum]["status"] == "ON":
									print "Node#%d is diconnected from the server" % (nodeNum)
									self.clientIpTable[nodeNum]["status"] = "OFF"
									self.OnlineNodeMap.pop(nodeNum)
									self.sendIpTable("remove", nodeNum)
									break
									
					except socket.error:
						inputs.remove(fd)
						for nodeNum in self.clientIpTable:
							if self.clientIpTable[nodeNum]["fd"] == fd and self.clientIpTable[nodeNum]["status"] == "ON":
								print "Node#%d is diconnected from the server" % (nodeNum)
								self.clientIpTable[nodeNum]["status"] = "OFF"
								self.OnlineNodeMap.pop(nodeNum)
								self.sendIpTable("remove", nodeNum)
								break
						self.lock.release()
						continue

					self.lock.release()


if __name__ == "__main__":
	sApp = server()
	sApp.serverLoop()


























	
